package game.board;
import game.*;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
public class BoardTest{


@Test
public void boardTest(){
  Board board = new Board(6, 7);
  assertNotNull(board);
}


@Test
public void getBoardSizeTest(){
    Board board=new Board(6, 7);
    assertEquals(42,board.getBoardSize());
}
}




/**
// ---Pour permettre l'exécution des test----------------------
   public static junit.framework.Test suite() {
       return new junit.framework.JUnit4TestAdapter(game.BoardTest.class);
   }
}**/