package game.resources;

import static org.junit.Assert.*;

import org.junit.Test;

public class RockTest {


    @Test
    public void checkIfValueIsCorrectTest() {
        Rocks r = new Rocks(2);
        assertEquals(2, r.getValue());
        Rocks r2 = new Rocks(5);
        assertEquals(5, r2.getValue());
    }
    
    @Test
    public void toStringTest(){
        
        Rocks r = new Rocks(2);
        assertEquals(2, r.getValue());
        assertTrue(r.toString().equals("Rock"));

    }
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(
            game.resources.RockTest.class
        );
    }
}
