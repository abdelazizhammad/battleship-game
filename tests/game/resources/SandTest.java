package game.resource;

import static org.junit.Assert.*;

import org.junit.Test;

public class SandTest {


    @Test
    public void checkIfValueIsCorrectTest() {
        Sand r = new Sand(2);
        assertEquals(2, r.getValue());
        Sand r2 = new Sand(5);
        assertEquals(5, r2.getValue());
    }
    
    @Test
    public void toStringTest(){
        
        Sand r = new Sand(2);
        assertEquals(2, r.getValue());
        assertTrue(r.toString().equals("Sand"));

    }
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(
            game.resource.SandTest.class
        );
    }
}
