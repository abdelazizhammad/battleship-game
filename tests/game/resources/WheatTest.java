package game.resource;

import static org.junit.Assert.*;

import org.junit.Test;

public class WheatTest {


    @Test
    public void checkIfValueIsCorrectTest() {
        Wheat r = new Wheat(2);
        assertEquals(2, r.getValue());
        Wheat r2 = new Wheat(5);
        assertEquals(5, r2.getValue());
    }
    
    @Test
    public void toStringTest(){
        
        Wheat r = new Wheat(2);
        assertEquals(2, r.getValue());
        assertTrue(r.toString().equals("Wheat"));

    }
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(
            game.resource.WheatTest.class
        );
    }
}
