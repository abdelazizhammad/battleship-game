package game.resource;

import static org.junit.Assert.*;

import org.junit.Test;

public class WoodTest {


    @Test
    public void checkIfValueIsCorrectTest() {
        Wood r = new Wood(2);
        assertEquals(2, r.getValue());
        Wood r2 = new Wood(5);
        assertEquals(5, r2.getValue());
    }
    
    @Test
    public void toStringTest(){
        
        Wood r = new Wood(2);
        assertEquals(2, r.getValue());
        assertTrue(r.toString().equals("Wood"));

    }
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(
            game.resource.WoodTest.class
        );
    }
}
