# l2s4-projet-2021

# Equipe

- Chaimae JEDDA
- Abir BEZZAZI
- Abdelaziz HAMMAD AHMED

# Sujet

[Le sujet 2021](https://www.fil.univ-lille1.fr/portail/index.php?dipl=L&sem=S4&ue=Projet&label=Documents)

# introduction 
ce projet est une modelisation de jeu et cette modelisation permet d'implanter deux differents jeux qui ont la meme base et des regles differents.
jeu de guere
jeu agrigole 

# Livrables

## Livrable 1
le livrable1 est une modélisation des personnages de jeu sous forme d'un uml de classe
les deux classes Army et Worker qui heritent  de la classe Character 
les deux classes PlayerWar et PlayerFarmer qui heritent de Player
### Atteinte des objectifs

Au début on n'était pas sur si on met la methode getFoodStock() sur la classe PlayerWar ou War Guerrier, donc la professeur nous a bien guidé.


### Difficultés restant à résoudre

## Livrable 2
le livrable2 est une modélisation des tuiles, resources et plateau de jeu.

### Atteinte des objectifs

Pour la représentation de la classe tuile on a pas trouvé de difficultés particulières, par contre comment définir la position on était un peu perdu vu qu'on voulait créer une autre classe à part (Position) mais au final on s'est contentés de Tile.

Pour ce qui est des ressources on a pensé à les représenter comme une classe enum cependant après avoir vu avec notre prof elle nous à redirigé plus vers une classe avec un héritage comme la classe tuile.

Le plateau, on l'a représenté comme étant une classe avec les params long et larg qui représentent les lignes et colonnes de notre board, avec l'attribut tuile de type tableau à deux dimensions.


### Difficultés restant à résoudre
 Pas de difficultés en vue pour le moment.
	
## Livrable 3
le livrable3 est une representation des actions des 2 jeux
avec une classe Game et une classe Main

### Atteinte des objectifs
pour la représentation des actions on a utilisé l'héritage   et on a mis une classe Main qui permet d'initialiser la classe Game 

Au début on a choisi la classe Action comme étant une interface qui regrouper toutes les actions du jeu, mais au final on est parti sur un héritage vu que ça va être plus visible et cohérent au moment du codage des classes.

### Difficultés restant à résoudre
Pas de difficultés en vue pour le moment.

## Livrable 4
le livrable4 c'est le dernier livrable et qui represente la modélisation finale du jeu qui rassemble tout les livrables precedents

### Atteinte des objectifs

La modélisation compléte du jeu est faite et on a pas eu de difficultés par rapport à ça.

Pour ce qui est du code ils nous reste quelques classes à compléter 

### Difficultés restant à résoudre

 Pas de difficultés en vue pour le moment.

# Journal de bord

## Semaine 1 
la premier semaine nous avons commencé a bien lire le sujet du projet et l'analyser 

## Semaine 2
nous avons fait notre premier diagramme uml qui modelise les personnages et on eu de classes character et Player avec leur heritage
## Semaine 3

On s'est concentré sur le code du Livrable 1 et parallèlement on mettais à jour notre diagramme en le complétant vu qu'on codant on voyait plus clair la modélisation du jeu

## Semaine 4
on a commencé a modeliser le livrable2 qui represente la modelisation du plateau, resources et tuiles

## Semaine 5
 
 on a terminé le codage des classes Player et Character avec leur heritage et en ce qui concerne le 1er livrable on a codé la plupart des methodes et ils nous restent quelque unes

## Semaine 6
on a commencé a modeliser le livrable 3 qui represente la classes action et la classe Game mais on a pas encore terminé le codage on a trouvé des problemes 
Changemenet d'interface à héritage pour la classe action

## Semaine 6
on se concentre sur le codage du livrable1 pour cette semaine on l'essaye de le terminer

## Semaine 7
on a terminé le livrable 3 des classes Actions et Game et on vient de commencer le code du livrable 2 on est en retard en codage

## Semaine 8
on code encore le livrable 2 les classes Actions et Game

## Semaine 9
on fait la modelisation finale du projet qui rassemble tous les umls du livrable1 jusqu'a livrable 3
## Semaine 10
on est concentré sur le codage puisque on a terminé les modelisation 
## Semaine 11
on code encore les classes actions et Game en parralelle on essaye de faire les tests et le makefile

## Semaine 12
on a terminé  le codage et on essaye de suivre les instructions pour le rendu pour etre dans le bon chemin 





