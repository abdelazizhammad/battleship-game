package game.tile;
import game.resources.*;

public class Forest extends Tile{

	public Forest(int xPos, int yPos) {
		super(xPos, yPos);
		this.name="Forest";
	}
	
	/** return the resource Wood
	 * 
	 * @return
	 */
	public Resources getResource() {
		Wood wood = new Wood();
		return wood;
	}
	
	/** return 2
	 * 
	 * @return
	 */
	public int getGold() {
		return 2;
	}
	public String getsymbol() {
		return "F";
	}
}
