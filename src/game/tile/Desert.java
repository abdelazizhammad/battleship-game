package game.tile;

import game.resources.*;

public class Desert extends Tile{

	public Desert(int xPos, int yPos) {
		super(xPos, yPos);
		this.name="Desert";
	}
	

	public Desert() {
		// TODO Auto-generated constructor stub
	}


	/** return the resource Sand
	 * 
	 * @return
	 */
	public Resources getResource() {
		Sand sand = new Sand();
		return sand;
	}
	/** return 5
	 * 
	 * @return
	 */
	public int getGold() {
		return 5;
	}
	public String getSymbol() {
		return "D";
	}
}
