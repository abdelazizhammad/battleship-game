package game.tile;
import game.resources.*;
public class Mountain extends Tile{

	public Mountain(int xPos,int yPos) {
		super(xPos, yPos);
		this.name="Mountain";
	}
	
	/** return the resource Rocks
	 * 
	 * @return rocks
	 */
	public Resources getResource() {
		Rocks rocks = new Rocks();
		return rocks;
	}
	
	/** return 8
	 * 
	 * @return
	 */
	public int getGold() {
		return 8;
	}
	public String getsymbol() {
		return "M";
	}
}
