package game.tile;

public class Ocean extends Tile{

	public Ocean(int xPos,int yPos) {
		super(xPos, yPos);
		this.name="Ocean";
	}
	public String getsymbol() {
		return "O";
	}

}
