package game.tile;
import game.resources.*;

public class Plain extends Tile{

	public Plain(int xPos,int yPos){
		super(xPos, yPos);
		this.name="Plain";
	}
	
    public String getName() {
        return this.name;
        
    }
	
	/** return the resource Wheat
	 * 
	 * @return
	 */
	public Resources getResource() {
		Wheat wheat = new Wheat();
		return wheat;
	}
	
	/** return 2
	 * 
	 * @return
	 */
	public int getGold() {
		return 2;
	}
	public String getsymbol() {
		return "P";
	}

}
