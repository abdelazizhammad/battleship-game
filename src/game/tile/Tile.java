package game.tile;
import game.character.Character;
import game.player.*;
import game.resources.*;

public class Tile {
	protected Character character;
	protected Player player;
	protected int xPos;
	protected int yPos;
	protected String name;
	protected Resources resource;
	protected boolean isOccupied;
	
	public Tile() {
		this.character = null;
		//isOccupied = false;
		this.xPos=null;
		this.yPos=null;
	}
	
	

	public String getName() {
		return this.name;
	}
	
	public void addCharacterOnTile(Character character) {
		this.character = character;
		//this.player=player;
	}
	
	public Character getCharacter() {
		return this.character;
	}
	
	public boolean isOccupied() {
		if(this.character != null) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/*
	public void setOccupied() {
		this.isOccupied = true;
		
	}
	*/
	
	public void unSetCharacter() {
		this.character = null;
	}
	public int getXPos() {
		return this.xPos;
	
	}
	
	public int getYPos() {
		return this.yPos=y;
		
	}
	
	
	public void changeXY(int x, int y) {
		this.xPos=x;
		this.yPos=y;
	}
	
	public boolean containsRess() {
		//if ()
		return false;
	}
	
	 public void emptyTheTile() {
		this.character=null;
		this.player=null;
		this.resource=null;
	}
}
