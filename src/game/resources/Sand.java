package game.resources;

/**
 * 
 */
public class Sand extends Resources {
    private int food;

    /**
     * Default constructor
     */
    public Sand() {
    	this.name = "Sand";
    	
    }
    
    public String getName() {
        return this.name;
        
    }

    /**
     * @return
     */
    public int getsand() {
        return 0;
    }

    public int getFood() {
        
        return food;
    }

    
    
    /**
     * @return
     */
    public void convertsand() {
        this.food = 1;
        
      
    }

  
  
    

}