package game.character;

import game.player.Player;
import game.tile.Tile;

public abstract class Character {
	protected int gold;
	protected Tile position;
	protected Player player;
	protected String id;
	protected int size;
	
	public Character(Player player, Tile position, int size){
		player=this.player;
		position=this.position;
        this.size = size;
	}
	public Tile getTile() {
		return this.position;
	}
	public int getGold() {
		return this.gold;
	}
	public String getPlayerName() {
		return this.player.getPlayerName();
	}
	public void addGold(int g){
		this.gold = this.gold + g;
	}
	public void substractGold(int g){
		this.gold = this.gold - g;
	}
	public String getId(){
		return this.id;
	}

    public int getSize() {
        return this.size;
    }

}
