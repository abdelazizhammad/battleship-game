package game.character;

import game.player.*;
import game.tile.*;

public class Army extends Character {
	
	private int nbWorriors;
	private boolean hunger;

	public Army(Player player, Tile position, int nbWorriors) {
		super(player, position, nbWorriors);
		nbWorriors=this.nbWorriors;
	}
	public void WinWorrior(int won) {
		this.nbWorriors+=won;
	}
	public void LoseWorrior(int lost) {
		this.nbWorriors-=lost;
	}
	public boolean isHungry(){
		return this.hunger;
	}

}
