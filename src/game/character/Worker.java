package game.character;

import game.player.*;
import game.tile.Tile;

public class Worker extends Character {
	private int food;
	public Worker(Player player, Tile position, int size) {
		super(player, position, size);
		gold=0;
		this.food=0;
	}
	public int getFood(){
		return this.food;
	}

}