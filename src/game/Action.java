package game;

import game.player.*;
import game.tile.*;
import game.resources.*;


public interface Action {
   
    public String toString(
        Character character,
        Player player,
        Tile tile,
        Resources resource
    );

    public void doAction(Character character, Player player) throws Exception;
}
