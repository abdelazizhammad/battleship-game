package game;

import java.util.*;

import game.actions.*;
import java.util.Iterator;
import java.util.List;
import game.board.*;
import game.player.*;

/**
 *
 */
public class Game {
	
    private ArrayList<Player> players;
    private Action act;
    private ArrayList<Action> action;

    private Board board;
    private int rounds;
    private int nbreCharacter;
    private String gametype;
	private Player winnerPlayer;
	private boolean can_play;
	private boolean is_over;
	private Player nextP;
	protected Deploy dep;
	protected DeployInFarm depF;
	private DeployInWar depw;
	private DoNothing dono;
	private Exchange exch;
	private Harvest harv;
	
    




    /**
     * @param Player player
     */

    public Game(Board board) {

      this.players = new ArrayList<Player>();
      this.board = board;
      
      this.rounds = 0;
      this.action = new ArrayList<Action>();
      this.action.add(this.dep);
      this.action.add(this.depF);
      this.action.add(this.depw);
      this.action.add(this.dono);
      this.action.add(this.exch);
      this.action.add(this.harv);
      


    }

    /**
     * @return
     */
    public void setGame(String gtype) {
    	
    	if(gtype == "war") {
    		this.gametype = "war";
    		this.rounds = 10;

    		
    	}
    	if(gtype == "farm") {
    		this.gametype = "farm";
    		this.rounds = 6;

    		
    	}
    	else {
    		System.out.println("Please set the game type to war or farm");
    	}
    }

    /**
     * @return
     */
    public void addPlayer(Player p) {
    	
        this.players.add(p);
    }

    /**
     * @return
     */
    public void play() {
    	
    	if(this.gametype == "war") {
    		if(this.rounds < 10) {
    			playround();
    		}
    		
    	}
    	if(this.gametype == "farm") {

    		if(this.rounds < 6) {playround();}
    	}
    	
    	else {
    		System.out.println("Please set the game type");
    	}
    }

    /**
     * @return
     */
    public boolean isOver() {
		if (this.rounds == 0) {
        	this.is_over = true;
        }
		return this.is_over;
    }

    /**
     * @return
     */
    public String getGameType() {
        
        return this.gametype;
    }
    
    
    public int getRoundsNm(Player pl) {
        return pl.getRoundNum();
    }
    
   


    /**
     * @return
     */
    public boolean canPlay(Player p) {
        if(p.getRoundNum() > 0 & this.getNextPlayer() ==p) {
            this.can_play =  true;
	
        }
		return this.can_play;
    }

    /**
     * @param Player p1
     * @param Player p2
     * @return
     */
    public void swapPlayer(Player p1, Player p2) {
    	p1=p2;
    	p2=p1;
    }

    /**
     * @return
     */
    public Player getNextPlayer() {
    	Iterator<Player> i = this.players.iterator();
        if (i.hasNext()) {
        	this.nextP = i.next();
        }
		return this.nextP;
		
    }



    /**
     * @return
     */
    public void playRound() {
  
        this.rounds = this.rounds -1;
    }

    /**
     * @return
     */
    public int getRound() {
        return this.rounds;
    }




   

    /**
     * @return
     */
    public void setAction() {
    	Random rand = new Random();
    	this.act = this.action.get( (rand.nextInt(((this.action.length-1) - 0) + 1) + 0) );
        
    }
    
    public Action getAction() {
    	return this.action ;
        
    }

    /**
     * @return
     */
    public void setWinner() {
    	for (int i = 0; i < this.players.size(); i++) {
    		if(this.players.get(i).getScore() > this.players.get(i+1).getScore()){
    			this.winnerPlayer = this.players.get(i);
    		}
    			
    	    }    
    }

    /**
     * @return
     */
    public Player getWinner() {
    	return this.winnerPlayer;
    	
    }

    /**
     * @return
     */
    public int getPlayersNum() {
        
        return this.players.size();
    }

    /**
     * @return
     */
    public String toString() {
        // TODO implement here
        return "";
    }



}