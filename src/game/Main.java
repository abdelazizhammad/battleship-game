package game;

import game.*;
import game.board.*;
import game.player.*;

/**
 *
 *
 * @author Abir & Chaimae & Abdelziz
 * @version 1.0
 */

 public class Main{
   public static void main(String [] argv){

    //Player p = new Player();
    Board b = new Board(10,10);
    Game g = new Game(p, 10, 10);
    int nbPlayers = 2;
    for (int t = 0;t < nbPlayers;t++){
      String namePlayer = "Joueur "+(t+1);
      g.addPlayer(new Player(namePlayer, t));
    }
    g.play();
 }
}