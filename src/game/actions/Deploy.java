package game.actions;

import game.*;
import game.board.*;
import game.character.Character;
import game.player.Player;
import game.resources.Resources;
import game.tile.Tile;
import game.util.MyRandom;

public abstract class Deploy implements Action {

    protected MyRandom random;
    protected Board board;

    
    public Deploy(Board board) {
        this.board = board;
        this.random = new MyRandom();
    }

    
    

    
    public abstract void doAction(Character character,Player player) throws Exception;

    
    public String toString(
        Character character,
        Player player,
        Tile tile,
        Resources resource
    ) {
        String chaine =
      
            player.getPlayerName() +
            " \n Vous deployez un/une " +
            character.getId() +
            " de taille" +
            " " +
            character.getSize() +
            " sur " +
            tile.getName();
        return chaine;
    }
}
