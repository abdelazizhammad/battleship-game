package game.player;
import game.character.*;
import game.character.Character;
import game.player.*;
import game.tile.*;
import game.board.*;
import game.resources.*;
import game.actions.*;


public class Player {

	protected int score ;
	protected String name ;
	protected Tile[ ] tile ;
	protected Action action ;
	protected Character[] character ;
	private int round_num;
	private int NUM_OF_CH;
	private int CH_LENGTHS[];

	public Player(String name, int rounds){

		this.name = name ;
		this.round_num = rounds;
		character = new Character[NUM_OF_CH];
		for (int i = 0; i < NUM_OF_CH; i++){

			Character temp_ch = new Character(CH_LENGTHS[i]);
			Character[i] = temp_ch;
		}
	}

	public String getPlayerName() {
		return this.name ;
  }

	public void setScore(int points) {
		this.score =  points ;
	}

  public void addScore(int points) {
		this.score =  this.score + points ;
	}

	public int getScore() {
		return this.score ;
	}



	public void collectRess() {

	}

  public void convertRess() {

	}

  public int getGold() {
		return this.gold ;
  }

	public int addGold(int g) {
		this.gold = this.gold + g ;
  }

  public void dropGold(int g) {
		this.gold = this.gold - g ;
  }



  public void chooseAction(Action action, Character p) {
		for (Character c: character){
			Board.addCharacter(p);
    }
	}

  public int getRoundNum(){
		return this.round_num;
	}

  public void decRoundNum(){
		this.round_num -= 1;
	}

	public String getPlayerPosition(){
		return toString();
	}


  public String toString(){
		return "   ";
	}





}
