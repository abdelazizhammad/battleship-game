package game.board;

import java.util.ArrayList;
import java.util.Random;
import game.tile.*;
<<<<<<< HEAD
import game.character.*;
import game.character.Character;
=======
>>>>>>> 719aacdc3fb06a71742a9d4c2ccb1b313c838274


/**
 *
 */


public class Board {

    protected int nbreCharacter;
    protected int length;
    protected int width;
    protected int size;
    protected int nmOceans;
    protected String[][] myBoard;
    protected Character[][] position;
    //protected String[] tiles;
    protected ArrayList<String> tiles; 
    protected String[] all_tiles ;
    protected String[] oceans ;
    protected boolean full;

    /**
     * @param int len
     * @param int wid
     */
    public Board(int len, int wid) {
   
        this.all_tiles = new String[size];
        this.oceans = new String[nmOceans];
        //this.tiles = { "M", "P", "D", "F"};

    
        this.length = len;
        this.width = wid;
        this.size = len*wid;
        this.nmOceans = ((len * wid)*2)/3;
        this.myBoard = new String[len][wid] ;
        this.position = new String[len][wid];
        this.all_tiles = new String[size];
        this.oceans = new String[nmOceans];
        //this.tiles = new Tile[4];
        this.tiles = new ArrayList<String>();
        this.tiles.add("M");
        this.tiles.add("P");
        this.tiles.add("D");
        this.tiles.add("F");       
        this.full = false;
        Random r = new Random();
        



        for(int j = 0; j < this.oceans.length; j++){
        	this.all_tiles[j] = "O";
         }

         for (int i = 0; i < len; i++){
            for(int j = 0; j < wid; j++){
            	this.myBoard[i][j] = this.all_tiles[r.nextInt(((this.size-1) - 0) + 1) + 0];

            }

         }


   

         for (int i = 0; i < len; i++){
            for(int j = 0; j < wid; j++){
              if(this.myBoard[i][j] != "O"){
            	  this.myBoard[i][j] = this.tiles.get( (r.nextInt(((this.tiles.size()-1) - 0) + 1) + 0) );
              }
            }
          }








          for (int i = 0; i < len; i++){
             for(int j = 0; j < wid; j++){

              if(this.myBoard[i][j] != "O"){

                  if((i != len -1 & j!= wid-1)&(i != 0 & j!=0)){
                   if( (this.myBoard[i][j+1]=="O") & (this.myBoard[i][j-1]=="O") & (this.myBoard[i-1][j]=="O") & (this.myBoard[i+1][j]=="O") ){
                	   this.myBoard[i][j] = "O";
               }
             }





                 if(i == 0){


                   if(j==0){
                     if((this.myBoard[1][0]=="O") & (this.myBoard[0][1]=="O")){this.myBoard[0][0] = "O";}
                   }

                   if(j==wid-1){
                     if((this.myBoard[1][wid-1]=="O") & (this.myBoard[0][wid-2]=="O")){this.myBoard[0][wid-1] = "O";}
                   }
                   if(j!=0 & j!= wid-1){
                     if((this.myBoard[0][j+1]=="O") & (this.myBoard[0][j-1]=="O") & (this.myBoard[1][j]=="O")){this.myBoard[0][j] = "O";}
                   }
                 }




                  if(i == len-1 ){
                   if(j==0){
                     if((this.myBoard[8][0]=="O") & (this.myBoard[9][1]=="O")){this.myBoard[len-1][0] = "O";}
                   }
                   if(j==wid-1){
                     if((this.myBoard[len-2][wid-1]=="O") & (this.myBoard[len-1][wid-2]=="O")){this.myBoard[len-1][wid-1] = "O";}
                   }
                   if(j!=len-1 & j!= 0){
                     if((this.myBoard[len-1][j+1]=="O") & (this.myBoard[len-1][j-1]=="O") & (this.myBoard[len-2][j]=="O")){this.myBoard[len-1][j] = "O";}
                   }
                 }




                 if (j==0 & (i !=0 & i != len-1) ){
                   if((this.myBoard[i][1]=="O") && (this.myBoard[i-1][0]=="O") && (this.myBoard[i+1][0]=="O")){this.myBoard[i][0] = "O";}
                 }
                 if(j==wid-1 & (i !=0 & i != len-1)){
                   if((this.myBoard[i][wid-2]=="O") & (this.myBoard[i-1][wid-1]=="O") & (this.myBoard[i+1][wid-1]=="O")){this.myBoard[i][wid-1] = "O";}}

              }
            }
      }





    }
    
    
    
   

    /**
     * @param int row
     * @param int col
     * @return
     */

    public String getStatus(int row, int col) {
    	return this.myBoard[row][col];
    }


    public int getLength() {
        return this.length;
    }

    public int getWidth() {
        return this.width;
    }

    public int getBoardSize() {
        return this.size;
    }

    public int getNmOceans() {
        return this.nmOceans;
    }

    public String[][] getBoard() {
        return this.myBoard;
    }
/*
    public String[] getTilesChar() {
        return this.tiles;
    }
    */

    public String[] getAllTiles() {
        return this.all_tiles;
    }

    public String[] getOceans() {
        return this.oceans;
    }


    public String printBoard() {
      for (int i = 0; i < this.length; i++) {
        for (int j = 0; j < this.width; j++) {
          System.out.print(this.myBoard[i][j]);
          System.out.print("  ");
        }
      System.out.println("\n");
     }


    }




    /**
     * @param int row
     * @param int col
     * @return
     */
    public boolean hasCharacter(int row, int col) {
        if(this.position[row][col] != null) {
        	return true;
        }
        else {
        	return false;
        }
    }

    /**
     * @return
     */
    public boolean isBoardFull() {
    	for (int i = 0; i < this.length; i++) {
          	for (int j = 0; j < this.width; j++) {
          		if (this.position[i][j] != null) {
          			this.full = true;
          		}
            }

          }
        return this.full;
    }

    /**
     * @param Character c
     * @return
     */
    public void addCharacter(Character c, int row, int col) {
    	this.position[row][col] = c;

    }

}
