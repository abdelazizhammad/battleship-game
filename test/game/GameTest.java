package game;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GameTest {

  private Game g;
  private Board b;
  private Player p1;
  private Player p2;

    @Before
    public void before() {
      this.p1 = new Player("Abir");
      this.p2 = new Player("Chaimae");
      this.b = new Board(5, 8);
      this.g = new Game(this.b);
    }

    @Test
    public void createTest(){
      assertNotNull(this.g);
    }

  

    @Test
    public void testGetNextPlayer(){
      this.g.addPlayer(this.p1);
      this.g.addPlayer(this.p2);
      assertSame(this.p1,this.g.nextPlayer());
    }

    @Test
    public void testCanPlay(){
      this.g.addPlayer(this.p1);
      assertTrue(this.g.canPlay(this.p1));
    }

    // ---Pour permettre l'execution des tests ----------------------
        public static junit.framework.Test suite() {
    	return new junit.framework.JUnit4TestAdapter(game.GameTest.class);
        }

    }
